EAPI="5"
PYTHON_COMPAT=( python3_{4,5,6} ) #test pypy

inherit distutils-r1

DESCRIPTION="Automate certificate signing request submission to the KIT-CA"
HOMEPAGE="https://git.scc.kit.edu/scc-net/certreq"
LICENSE="MIT"

SLOT="0"
DEPEND="${PYTHON_DEPS} dev-python/python-distutils-extra[${PYTHON_USEDEP}] virtual/pkgconfig"

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="https://git.scc.kit.edu/as5135/certreq"
	SRC_URI=""
else
	SRC_URI="https://git.scc.kit.edu/as5135/certreq/-/archive/v${PV}/certreq-v${PV}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

IUSE=""

RDEPEND="${DEPEND} dev-python/suds[${PYTHON_USEDEP}]"

src_compile() {
	distutils-r1_src_compile
}

src_install() {
	distutils-r1_src_install
}

